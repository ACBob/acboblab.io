/*
	Little, extremely simple script that adds a copy button to syntax hilighted
	code blocks. This is FINE within my philosophy of this site, as it's not
	important logic but is a nice QOL thing.
*/


document.addEventListener( 'DOMContentLoaded', () => {
	const blocks = document.querySelectorAll( '.hll' );

	blocks.forEach( ( block ) => {
		const button = document.createElement( 'button' );
		button.classList.add( "copy-button" );
		button.addEventListener( 'click', async () => {
			// we want to select it all so even *if* copying fails,
			// they can still copy by hand.
			const range = document.createRange();
			const selection = window.getSelection();
			range.selectNodeContents( block );
			selection.removeAllRanges();
			selection.addRange( range );

			// First we try the newer clipboard api (which I couldn't get to work consistently)
			try
			{
				await navigator.clipboard.writeText( block.innerText );
			}
			catch ( e )
			{
				// Then we try the old version
				try
				{
					document.execCommand( "copy" );
				}
				catch ( e1 )
				{
					// Ah well
					console.error( "Copying Error:", e, e1 );
				}
			}
		} );

		block.appendChild( button );
	} );
} );