'use strict';

/*
        This is perhaps the simplest experiment so far.
        It's literally just converting to seconds and back from.
        It's so easy.
*/

// Commonly used for conversions
const MINUTE = 1 * 60.0;
const HOUR   = MINUTE * 60.0;
const DAY    = HOUR * 24.0;
const WEEK   = DAY * 7.0;
const YEAR   = DAY * 365.0;

// We don't actually let people convert to/from months (they're not linear)
// but we can approximate then to be 28 days evenly (4 weeks) and use that for
// good enough monthly based times
const MONTH = DAY * 28;

const UNITS = {
	// Each entry should look such:
	'seconds': {
		'label': 'Earth Seconds', // Displayed in option box
		'seconds': 1,             // How many of these are in a second
	},

	'minutes': { 'label': 'Earth Minutes', 'seconds': MINUTE },
	'hours': { 'label': 'Earth Hours', 'seconds': HOUR },
	'days': { 'label': 'Earth Days', 'seconds': DAY },
	'weeks': { 'label': 'Earth Weeks', 'seconds': WEEK },
	'years': { 'label': 'Earth Years', 'seconds': YEAR },

	// https://en.wikipedia.org/wiki/Caesium_standard
	'caesium': { 'label': 'Caesium Standards', 'seconds': 1 / 9_192_631_770 },

	// https://www.youtube.com/watch?v=-KFH--_cdiI?t=87
	// Just called 'fortnights' so the 'fortnites' humor works, but is technically
	// Earth Fortnights
	'fortnight': { 'label': 'Fortnights', 'seconds': WEEK * 2.0 },

	// As of nov 2023, your average fortnite season (period of themed content?)
	// lies at 85 days.
	'fortnite': { 'label': 'Fortnites', 'seconds': DAY * 85 },

	// common saying is that dog years are 7x shorter than human years
	// extrapolating linearly, all dog time is 7 times shorter
	'dogyear': { 'label': 'Dog Years', 'seconds': YEAR / 7 },
	'dogsecond': { 'label': 'Dog Seconds', 'seconds': 1 / 7.0 },
	'dogminute': { 'label': 'Dog Minutes', 'seconds': MINUTE / 7.0 },
	'doghour': { 'label': 'Dog Hours', 'seconds': HOUR / 7.0 },
	'dogdays': { 'label': 'Dog Days', 'seconds': DAY / 7.0 },
	'dogweek': { 'label': 'Dog Weeks', 'seconds': DAY }, // 7 days divided by 7 is 1 day

	// Based on fan interpretation of a prop from a non-canon film that
	// canonicised rels as a unit of time rather than energy output.
	// Doctor Who is weird.
	'rels': { 'label': 'Dalek Rels', 'seconds': 1.2 },

	// The whole reason for this calculator, a simple shower thought;
	// "What if you measured time in pregnancy trimesters?"
	// a trimester seems to be 12 weeks.
	'trimester': { 'label': 'Trimesters', 'seconds': WEEK * 12.0 },

	// I imagine this is a painfully huge number.
	// Thankfully javascript just makes it work.
	'galyear': { 'label': 'Galactic Year', 'seconds': YEAR * 250_000_000 },

	// average, it was a sundial measurement so it varied!
	'moment': { 'label': 'Medieval Moments', 'seconds': 90 },

	// 1 hr 44 minutes
	'morbs': { 'label': 'Morbius (2022)', 'seconds': HOUR + MINUTE * 44 },

	// 10 nanoseconds
	'shake': { 'label': 'Shake', 'seconds': 1e-8 },

	// 20 minutes, old english word
	// sounds like a funny rebuttal to lightyear however
	'mileway': { 'label': 'Mileway', 'seconds': MINUTE * 20.0 },

	'american': {
		'label': 'Superbowls',
		// https://www.theverge.com/2017/2/4/14403598/how-long-is-the-super-bowl
		// 3 hours, 44 minutes
		'seconds': 3 * HOUR + 44 * MINUTE,
	},

	// https://www.radiotimes.com/tv/sci-fi/how-long-does-it-take-to-watch-every-episode-of-doctor-who/
	// 28 days 7 hours and 50 minutes
	// probably doesn't include the 1996 film, so we add 89 minutes
	'doctor': {
		'label': 'Doctor Who (1963-1989, 1996, 2005-2023, 2023-)',
		'seconds': 28 * DAY + 7 * HOUR + 50 * MINUTE + 89 * MINUTE,
	},

	'whht': { 'label': 'William Henry Harrison Terms', 'seconds': DAY * 31 },

	// A unit of time that is typically exactly 1 hour and 35 minutes 32 seconds,
	// the same length as the movie, Shrek (2001). As the movie will be an
	// additional second in length for certain DVD players, which is approximately
	// one in every 9.807 DVDs, Leap Shreks™ must be included when measuring time
	// with this unit. One Leap Shrek™ is equal to 95 minutes and 33 seconds, and
	// occurs once every 9.807 (typically rounded to 9.81 or 10) Shreks™.
	// Leap seconds are a stupid thing I'm not doing them (for now... maybe if
	// conversions can have custom functions?)
	'shrek': { 'label': 'Shrek (2001)', 'seconds': MINUTE * 95 + 33 },

	// Remember vine?
	'vine': { 'label': 'Vines', 'seconds': 6 },

	// Inside joke, ish
	'emmittan': {
		'label': 'Emmittans',
		'seconds': 1 / 2.835,
	},

	// Would be more complicated but funny
	//   'microwave': {
	//     'label': 'Microwave Time',
	//     'function': (t) => `${Math.round(t / 60.0)}:${t % 60.0}`,
	//   }

	'yorksecond': { 'label': 'New York Second', 'seconds': 1 / Infinity },

	'craftday': { 'label': 'Minecraft Day', 'seconds': MINUTE * 20 },
	// a lunar cycle is 8 days
	'craftmoon': { 'label': 'Minecraft Moon', 'seconds': MINUTE * 20 * 8 },

	// 1 chain is 20.1168 meters, a chain second is the same calculation.
	// This isn't a standard or previously established, I just thought it funny.
	'chainsecond': { 'label': 'Chain Second', 'seconds': 20.1168 },

	// Smokers live, on average, 13 years less than the average person.
	// The world's average lifespan (as of writing) is 73.4 years.
	'smoker': { 'label': 'Smoker', 'seconds': YEAR * 60.4 },

	'fnaf': { 'label': 'Nights at Freddy\'s', 'seconds': HOUR * 6 },

	// Stream Ego Orb
	'rocco': { 'label': 'Rocco Rocco', 'seconds': 2 * MINUTE + 50 },
	'roccorocco': { 'label': 'Rocco Rocco (Rocco)', 'seconds': 2 * MINUTE + 49 },

	// According to wikiped, the average male refactory is 30 minutes
	'refactory': { 'label': 'Male Refactory Periods', 'seconds': MINUTE * 30 },

	// Southpark 22.3 year rule
	'tragedy': { 'label': 'Comedies', 'seconds': YEAR * 22.3 },

	'yad': { 'label': 'syaD htraE', 'seconds': -DAY },

	// war units
	// The war on terror was a period of time between 14 sep 01 and 30 aug 21
	'terror': { 'label': 'Wars on Terror', 'seconds': 19.91 * YEAR + 2 * WEEK + 2 * DAY },
	'vietnam': { 'label': 'Vietnam Wars', 'seconds': 19.41 * YEAR + 4 * WEEK + DAY },
	'ww1': { 'label': 'First World Wars', 'seconds': 4.25 * YEAR + 2 * WEEK },
	'ww2': { 'label': 'Second World Wars', 'seconds': 6 * YEAR + DAY },

	'mtick': { 'label': 'Minecraft Ticks', 'seconds': 1 / 20.0 },
	'qtick': { 'label': 'Quake Ticks', 'seconds': 1 / 10.0 },
	'dtic': { 'label': 'Doom Realtics', 'seconds': 1 / 35.0 },
	'cstick': { 'label': 'CS:GO Ticks', 'seconds': 1 / 64.0 },

	// Interstellar
	// 1 hour is seven years
	// HOUR / (YEAR*7)
	'millersecond': { 'label': 'Miller Seconds', 'seconds': 61320.0 },
	'millerminute': { 'label': 'Miller Minutes', 'seconds': 61320.0 * MINUTE },
	'millerhour': { 'label': 'Miller Hours', 'seconds': 61320.0 * HOUR },
	'millerday': { 'label': 'Miller Days', 'seconds': 61320.0 * DAY },
	'millerweek': { 'label': 'Miller Weeks', 'seconds': 61320.0 * WEEK },
	'milleryear': { 'label': 'Miller Years', 'seconds': 61320.0 * YEAR },

	// Inception (a very boring movie)
	// The calcualtion is around 20 times
	// https://scifi.stackexchange.com/questions/36021/is-there-an-actual-time-calculation-in-inception
	"inceptseconds": { "label": "Dream seconds", "seconds": 1 / 20.0 },
	"inceptminutes": { "label": "Dream minutes", "seconds": MINUTE / 20.0 },
	"incepthours": { "label": "Dream hours", "seconds": HOUR / 20.0 },
	"inceptdays": { "label": "Dream days", "seconds": DAY / 20.0 },
	"inceptweeks": { "label": "Dream weeks", "seconds": WEEK / 20.0 },
	"inceptyears": { "label": "Dream years", "seconds": YEAR / 20.0 },

	"inceptedseconds": { "label": "Dream-dream seconds", "seconds": 1 / 20.0 / 20.0 },
	"inceptedminutes": { "label": "Dream-dream minutes", "seconds": MINUTE / 20.0 / 20.0 },
	"inceptedhours": { "label": "Dream-dream hours", "seconds": HOUR / 20.0 / 20.0 },
	"incepteddays": { "label": "Dream-dream days", "seconds": DAY / 20.0 / 20.0 },
	"inceptedweeks": { "label": "Dream-dream weeks", "seconds": WEEK / 20.0 / 20.0 },
	"inceptedyears": { "label": "Dream-dream years", "seconds": YEAR / 20.0 / 20.0 },

	"inceptededseconds":
		{ "label": "Dream-dream-dream seconds", "seconds": 1 / 20.0 / 20.0 / 20.0 },
	"inceptededminutes":
		{ "label": "Dream-dream-dream minutes", "seconds": MINUTE / 20.0 / 20.0 / 20.0 },
	"inceptededhours": { "label": "Dream-dream-dream hours", "seconds": HOUR / 20.0 / 20.0 / 20.0 },
	"inceptededdays": { "label": "Dream-dream-dream days", "seconds": DAY / 20.0 / 20.0 / 20.0 },
	"inceptededweeks": { "label": "Dream-dream-dream weeks", "seconds": WEEK / 20.0 / 20.0 / 20.0 },
	"inceptededyears": { "label": "Dream-dream-dream years", "seconds": YEAR / 20.0 / 20.0 / 20.0 },

	// Mario level timer is not in seconds
	// number below is based on the first Super Mario Bros for the NES.
	'mariosecond': { 'label': 'Mario Seconds', 'seconds': 0.4 },

	// Brexit means brexit, though even given 4 years we'll still leave without a
	// plan for money.
	// What a strong economy we have, with wartime prices for cheese!
	'brexit': { 'label': 'Brexits', 'seconds': DAY * 1317.0 },

	// Lol, outlasted by TF2 with no updates (though only because of community,
	// HL1 deathmatch has the same claim!)
	'overwatch': { 'label': 'Overwatches', 'seconds': DAY * 2323 },

	// Time is money, as they say...
	// US Federal minimum wage, Nov 2023
	// A livable wage would be in the 20$ an hour range btw, the fight for 15$ has
	// been that long!
	'usd': { 'label': 'Dollars', 'seconds': HOUR / 7.25 },
	// £10.42 an hour for 23+, 5.28 an hour for <18
	'gbp': { 'label': 'Pounds', 'seconds': HOUR / 10.42 },

	// mentioned by the 6th doctor as a throwaway in "The Two Doctors",
	// apparently the time a pin galaxy (galaxy within an atom) exists for,
	// 1 quintillionth of a second
	// I've assumed an old british quintillion
	'atosecond': { 'label': 'Ato-Seconds', 'seconds': 1e-30 },

	// jokes that we do not calculate
	'lightyear': { 'label': 'Lightyears', 'no': true },
}

let unitSelectorFrom = document.getElementById( 'unit-select-from' );
let unitSelectorTo   = document.getElementById( 'unit-select-to' );
let timeSelector     = document.getElementById( 'time' );
let output           = document.getElementById( 'output' );

function populateUnits()
{
	let unitDatalistFrag = document.createDocumentFragment();

	// Populate based on the label sorting
	Object.keys( UNITS ).sort(
							( a, b ) => UNITS[a].label.localeCompare( UNITS[b].label )
	).forEach( unit => {
		let option   = document.createElement( 'option' );
		option.value = unit;
		option.text  = UNITS[unit].label;

		unitDatalistFrag.appendChild( option );
	} );

	// we clone here to avoid making multiple by hand :3
	unitSelectorFrom.appendChild( unitDatalistFrag.cloneNode( true ) );
	unitSelectorTo.appendChild( unitDatalistFrag );
}

function calculate( t, from, to ) { return ( t * UNITS[from].seconds ) / UNITS[to].seconds; }

// https://stackoverflow.com/a/32229831
function toNeccesaryDecimalsUpTo( value, dp ) { return +parseFloat( value ).toFixed( dp ); }

function runTheThings()
{
	let unitFrom = unitSelectorFrom.value;
	let unitTo   = unitSelectorTo.value;

	if ( UNITS[unitFrom].no )
	{
		output.textContent = `${UNITS[unitFrom].label} are not a unit of time!`;
		return;
	}
	if ( UNITS[unitTo].no )
	{
		output.textContent = `${UNITS[unitTo].label} are not a unit of time!`;
		return;
	}

	let time = timeSelector.value;

	if ( time == null || time == undefined || time == '' ) time = 0;

	if ( isNaN( time ) )
	{
		output.textContent = 'NaN!';
		return;
	}
	time = Number( time );

	let value = ( unitFrom == unitTo ) ? time : calculate( time, unitFrom, unitTo );

	// make it readable
	time  = toNeccesaryDecimalsUpTo( time, 5 );
	value = toNeccesaryDecimalsUpTo( value, 5 );

	output.textContent = `${time} ${UNITS[unitFrom].label} are ${value} ${UNITS[unitTo].label}.`;
}

// Populate the unit list
populateUnits();

// Connect the stuff
unitSelectorFrom.addEventListener( 'input', runTheThings );
unitSelectorTo.addEventListener( 'input', runTheThings );
timeSelector.addEventListener( 'input', runTheThings );

// set to seconds first if time selector is empty, that probably means the
// content hasn't been left over and this is a new load
// I will not use cookies!
if ( timeSelector.value == '' ) { unitSelectorFrom.value = unitSelectorTo.value = 'seconds'; }
runTheThings();