/**
 * A bunch of helpful utility things for WebGL.
 */

/**
 * Performs some initialisation of the gl context.
 * @param {WebGLRenderingContext} gl
 */
function initWebGL( gl ) { gl.clearColor( 0.0, 0.5, 1.0, 1.0 ); }

/**
 *
 * @param {WebGLRenderingContext} gl
 * @param {String} vertexSource Vertex shader source code
 * @param {String} fragmentSource Fragment shader source code
 * @returns {WebGLProgram|undefined} The compiled shader program, or error.
 */
function compileShader( gl, vertexSource, fragmentSource )
{
	// NOTE: We can rely on linking failing if either shader fails to compile.
	// On firefox, the error message contained which shader failed to compile
	// causing bad linkage.

	// WebGL is quite verbose. At-least it's not vulkan!
	const vertShader = gl.createShader( gl.VERTEX_SHADER );
	gl.shaderSource( vertShader, vertexSource );
	gl.compileShader( vertShader );

	const fragShader = gl.createShader( gl.FRAGMENT_SHADER );
	gl.shaderSource( fragShader, fragmentSource );
	gl.compileShader( fragShader );

	// Linking stage
	const program = gl.createProgram();
	gl.attachShader( program, vertShader );
	gl.attachShader( program, fragShader );
	gl.linkProgram( program );

	// we can delete the shaders
	gl.deleteShader( vertShader );
	gl.deleteShader( fragShader );

	if ( !gl.getProgramParameter( program, gl.LINK_STATUS ) )
	{
		console.error( `Failed to link shader program: ${gl.getProgramInfoLog( program )}` );

		gl.deleteProgram( program );
		return null;
	}

	return program;
}

function isPow2( n ) { return ( n && !( n & ( n - 1 ) ) ); }

/**
 * Loads a texture from path.
 * @param {WebGLRenderingContext} gl
 * @param {String} path
 * @returns {WebGLTexture|undefined} Texture, or error
 */
function loadTexture( gl, path )
{
	const texture = gl.createTexture();
	gl.bindTexture( gl.TEXTURE_2D, texture );

	// We'll want to download it, so we set it to a pure green texture for now.
	gl.texImage2D( gl.TEXTURE_2D, 0, gl.RGBA, 1, 1, 0, gl.RGBA, gl.UNSIGNED_BYTE, new Uint8Array( [
					   0,
					   255,
					   0,
					   255,
				   ] ) );

	const image  = new Image();
	image.onload = () => {
		// Update the texture information
		gl.bindTexture( gl.TEXTURE_2D, texture );
		gl.texImage2D( gl.TEXTURE_2D, 0, gl.RGB, gl.RGB, gl.UNSIGNED_BYTE, image );

		if ( isPow2( image.width ) && isPow2( image.height ) )
		{
			gl.generateMipmap( gl.TEXTURE_2D );
		}

		gl.texParameteri( gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE );
		gl.texParameteri( gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE );
		gl.texParameteri( gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST );
		gl.texParameteri( gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST );
	};
	image.src = path;

	return texture;
}

export {initWebGL, compileShader, loadTexture};