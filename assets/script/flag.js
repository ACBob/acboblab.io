'use strict';

const FLAG_RATIOS = [
	0.82, // 5000000000:4101687939, Nepal
	1,    // 1:1, Switzerland
	1.5,  // 2:3, 85 nations
	2,    // 1:2 54 nations
];
const FLAG_RATIO_WEIGHTS = [1, 3, 8, 5];
const FLAG_WIDTH         = 1024;

const FLAG_EASTEREGGS = {
	"scotland": ( ctx, width, height ) => {
		ctx.fillStyle = '#008';
		ctx.fillRect( 0, 0, width, height );
		drawFlagCross( ctx, '#fff', '#fff', width / 8.0, width, height );

		return "Scotland";
	},

	"trans": ( ctx, width, height ) => {
		drawFlagStripes(
			ctx, true, ['#5BCEFA', '#F5A9B8', '#FFFFFF', "#F5A9B8", "#5BCEFA"], width, height
		);
		return "Translandia";
	},

	"barcode": ( ctx, width, height ) => {
		drawFlagStripes(
			ctx,
			false,
			[
				"#257e00", "#fff",    "#ff6a00", "#21007f", "#fff",    "#fe0004", "#fff",
				"#21007f", "#257e00", "#fe0004", "#fe0004", "#fed005", "#fe0004", "#21007f",
				"#fff",    "#fe0004", "#000",    "#fed005", "#fe0004", "#fe0004", "#fff",
				"#004a7f", "#fe0004", "#fff",    "#004a7f", "#000",    "#fe0004", "#fed005",
				"#257e00", "#fff",    "#fe0004", "#fe0004", "#fff",    "#fe0004", "#fe0004",
				"#fff",    "#fe0004", "#004a7f", "#fed005", "#004a7f", "#fff",    "#fff",
				"#21007f", "#fff",    "#fff",    "#004a7f", "#fff",    "#004a7f", "#fff",
				"#004a7f", "#fff",    "#004a7f",
			],
			width,
			height
		);
		return "Bad Decisions";
	},

	"ace": ( ctx, width, height ) => {
		drawFlagStripes( ctx, true, ['#000', '#aaa', '#fff', '#8548A6'], width, height );
		return "Denmark"; // Because ace people could totally overthrow denmark
	},

	"minecraft": ( ctx, width, height ) => {
		drawFlagStripes( ctx, false, ['#96725b', '#5b3822'], width, height );
		drawFlagTriangle( ctx, '#12893b', 0.5, 2, width, height );
		return "Gamer Land";
	},

	"ena": ( ctx, width, height ) => {
		drawFlagStripes( ctx, false, ['#004a7f', '#fed005'], width, height );
		drawFlagTriangle( ctx, '#000', 0.25, 0, width, height );
		drawFlagTriangle( ctx, '#000', 0.25, 1, width, height );
		drawFlagTriangle( ctx, '#000', 0.25, 2, width, height );
		return "Salutations!";
	},

	"larcson": ( ctx, width, height ) => {
		// Grid
		let gx = width / 16, gy = height / 16;

		// Draw hair
		ctx.fillStyle = '#62483d';
		ctx.fillRect( Math.floor( width / 2 ), 0, Math.ceil( width / 2 ), height );
		ctx.fillStyle = '#8e735e';
		ctx.fillRect( 0, 0, Math.ceil( width / 2 ), height );

		// Draw face
		ctx.fillStyle = '#e5b785';
		ctx.fillRect( 0, gy * 2, width, height );

		// Draw clothes
		ctx.fillStyle = '#ffdb58';
		ctx.fillRect( 0, gy * 6, width, height );
		ctx.fillStyle = '#7f001f';
		ctx.fillRect( gx * 3, gy * 6, width - gx * 6, height );
		ctx.fillStyle = '#00006c';
		ctx.fillRect( 0, gy * 14, width, height );

		// eyeballs
		ctx.fillStyle = '#199530';

		ctx.beginPath();
		ctx.arc( gx * 3, gy * 4, gx, 0, Math.PI * 2 );
		ctx.arc( width - gx * 3, gy * 4, gx, 0, Math.PI * 2 );
		ctx.fill();
		ctx.closePath();

		return "Neep!";
	},

	"states": ( ctx, width, height ) => {
		ctx.fillStyle = '#000';
		ctx.fillRect( 0, 0, width, Math.ceil( height / 2 ) );
		ctx.fillStyle = '#c00';
		ctx.fillRect( 0, Math.floor( height / 2 ), width, Math.ceil( height / 2 ) );
		ctx.fillStyle = '#ff0';

		ctx.beginPath();
		ctx.arc( width / 2, height / 2, width / 6, 0, Math.PI * 2 );
		ctx.fill();
		ctx.closePath();
		return "Max Barry";
	},

	// TODO: A few gaps and I don't like the clouds.
	"tmfa": ( ctx, width, height ) => {
		let gx = width / 4, gy = height / 4;
		ctx.fillStyle = '#a7318b';
		ctx.fillRect( 0, 0, width, height );

		ctx.fillStyle = '#ffcb21';
		ctx.beginPath();
		ctx.arc( gx, height / 2, gx * 0.75, 0, Math.PI * 2 );
		ctx.fill();
		ctx.closePath();

		ctx.fillStyle = '#65e603';
		ctx.fillRect( 0, gy * 2.5, width, gy * 1.5 );

		// smaller hill
		ctx.fillStyle = '#56b30f';
		ctx.beginPath();
		ctx.moveTo( width * 0.4, gy * 2.5 );
		ctx.lineTo( width * 0.6, gy * 1.5 );
		ctx.lineTo( width * 0.8, gy * 2.5 );
		ctx.fill();
		ctx.closePath();

		// bigger hill
		ctx.fillStyle = '#65e603';
		ctx.beginPath();
		ctx.moveTo( width * 0.5, gy * 2.5 );
		ctx.lineTo( width * 0.75, gy );
		ctx.lineTo( width, gy * 2.5 );
		ctx.fill();
		ctx.closePath();

		// draw clouds
		ctx.fillStyle = '#fff';
		ctx.beginPath();
		ctx.moveTo( 0, 0 );
		for ( let i = 0; i < width; i++ )
		{
			ctx.lineTo( i, gy * 0.125 + Math.abs( Math.sin( ( i / width ) * 16 ) ) * gy * 0.25 );
		}
		ctx.lineTo( width, 0 );
		ctx.lineTo( 0, 0 );
		ctx.fill();
		ctx.closePath();

		return 'Turobula';
	},
}

// Random colours that could be picked for painting flag elements
// (Stripes, outlines, emblems)
const FLAG_COLOURS_PREDETERMINED = [
	// Trans Rights
	'#5BCEFA',
	'#F5A9B8',
	'#FFFFFF',

	// Intersexy / Nonbinie :3
	'#FFF200',
	'#8548A6',
	'#2C2C2C',
];

function randomColour( rng )
{
	// https://stackoverflow.com/a/5365036 (Comment 1)
	if ( rng.Scalar() <= 0.75 ) return '#' + ( rng.Scalar() * 0xFFFFFF << 0 ).toString( 16 );
	else return rng.Choose( FLAG_COLOURS_PREDETERMINED );
}

// For generatin' country names
const COUNTRY_NAME_BEGINNINGS = [
	"aero",  "ala",  "aqua",  "auri", "bell", "boro", "cael", "cal",   "civ",  "cres",
	"dra",   "eco",  "eri",   "exo",  "flor", "ful",  "gal",  "gen",   "gra",  "heli",
	"hydra", "igni", "il",    "iri",  "kilo", "luna", "lux",  "mar",   "mer",  "mira",
	"nebu",  "neo",  "ner",   "novi", "nym",  "oce",  "octa", "orbi",  "ori",  "ovo",
	"pac",   "pan",  "phe",   "plu",  "pris", "pyr",  "quar", "quin",  "radi", "regi",
	"rho",   "sed",  "selen", "sil",  "sol",  "sono", "spec", "stel",  "sub",  "sum",
	"sun",   "tang", "terra", "tit",  "tran", "tri",  "trop", "umbra", "uni",  "uran",
	"vega",  "vent", "ver",   "ves",  "vio",  "vul",  "xeno", "zel",
];
const COUNTRY_NAME_MIDDLES =
	["and", "ar", "at", "ea", "el", "en", "er", "ic", "in", "is", "iv", "ix", "on", "or", "ul"];
const COUNTRY_NAME_ENDINGS = [
	"stan",  "ria",   "land",   "ville",  "tonia", "aria", "donia",     "onia",  "nia",   "tica",
	"ala",   "aria",  "anda",   "onesia", "aguay", "uda",  "osia",      "ovia",  "yria",  "ica",
	"uania", "nesia", "ovakia", "arus",   "alvia", "orea", "oslovakia", "aguay", "uania",
];
const COUNTRY_NAME_PREFIXES =
	['', 'New ', 'North ', 'South ', 'East ', 'West ', 'Old ', 'Ancient '];
const COUNTRY_NAME_SUFFIXES = ['', 'ia', 'land', 'stan', 'ville', 'topia', 'landia', 'mark'];

const COUNTRY_MOTTO_NOUNS = [
	"freedom",  "god",       "soil",     "honour",      "peace",        "order",
	"fascism",  "solar",     "welfare",  "nationalism", "unity",        "justice",
	"liberty",  "dignity",   "strength", "prosperity",  "heritage",     "culture",
	"progress", "diversity", "equality", "democracy",   "independence", "solidarity",
	"truth",    "love",      "glory",    "happiness",   "harmony",      "faith",
];

const COUNTRY_MOTTO_VERBS = [
	"strive",  "succeed", "enrich",    "persist",  "free",   "protect",    "preserve",
	"uphold",  "promote", "celebrate", "enslave",  "unite",  "inspire",    "achieve",
	"embrace", "empower", "foster",    "champion", "defend", "emancipate", "cherish",
];

const COUNTRY_MOTTO_ADJECTIVES = [
	"brave",      "just",   "strong", "mighty",     "glorious",    "peaceful",    "resilient",
	"prosperous", "stolen", "great",  "courageous", "determined",  "adventurous", "majestic",
	"noble",      "wise",   "proud",  "radiant",    "enlightened",
];
const COUNTRY_MOTTO_FORMS = [
	"For %n",
	"In %n we %v!",
	"%n, %n, %n.",
	"Towards a %a %n",
	"%n, %v, %n!",
	"Building a %a %n",
	"%v for %n",
	"%n first, %v always",
	"Ever onward, %n!",
	"%n, %v, %n, %v, %n.",
];

const FLAG_STYLE_STRIPED = 0;
const FLAG_STYLE_CROSS   = 1;
const FLAG_STYLE_PLUS    = 2;
const FLAG_STYLE_JACKOFF = 3;
const FLAG_STYLE_TRIS    = 4;
const FLAG_STYLE_MAX     = 4;

let flagCanvas         = document.getElementById( 'flagvas' );
let flagCtx            = flagCanvas.getContext( '2d' );
let countryNameDisplay = document.getElementById( 'countryname' );

let seedInput = document.getElementById( 'seed' );

function stringToSeed( str )
{
	// if the string is empty, just return 1
	if ( typeof str == 'undefined' || str.length <= 0 ) return 1;
	// numbers work fine as seeds, interpret them exactly
	if ( !isNaN( str ) ) return str;

	let hash = 0;
	if ( str.length === 0 ) return hash;
	for ( let i = 0; i < str.length; i++ )
	{
		let char = str.charCodeAt( i );
		hash     = ( ( hash << 5 ) - hash ) + char;
		hash     = hash & hash; // Convert to 32bit integer
	}
	return Math.abs( hash );
}

// Slight overkill
class RNG
{
	constructor( seed ) { this.Seed( seed ); }

	Seed( seed ) { this.state = seed || Date.now() || 1; }

	Scalar()
	{ // basic xor-shift random
		let x = this.state;
		x ^= x << 13;
		x ^= x >> 17;
		x ^= x << 5;
		this.state = x;
		return ( x >>> 0 ) / 4294967296;
	}

	GetInt( l, h ) { return Math.floor( this.Scalar() * ( h - l + 1 ) ) + l; }
	Get( l, h ) { return this.Scalar() * ( h - l ) + l; }
	Choose( a ) { return a[this.GetInt( 0, a.length - 1 )]; }

	ChooseWeighted( opts, weights )
	{
		const totalWeight = weights.reduce( ( acc, weight ) => acc + weight, 0 );
		let randomNum     = this.Scalar() * totalWeight;
		let index         = 0;
		while ( randomNum > weights[index] )
		{
			randomNum -= weights[index];
			index++;
		}
		return opts[index];
	}
};

function generateCountryName( rng )
{
	// Generate a name
	let name = rng.Choose( COUNTRY_NAME_BEGINNINGS );
	let mids = rng.GetInt( 1, 3 );
	for ( let i = 0; i < mids; i++ ) name += rng.Choose( COUNTRY_NAME_MIDDLES );
	name += rng.Choose( COUNTRY_NAME_ENDINGS );

	// Add prefix/suffix
	if ( rng.Scalar() < 0.5 ) name = rng.Choose( COUNTRY_NAME_PREFIXES ) + name;
	if ( rng.Scalar() < 0.5 ) name += rng.Choose( COUNTRY_NAME_SUFFIXES );

	// Transform to title case
	// https://stackoverflow.com/a/196991
	return name.replace(
		/\w\S*/g, ( f ) => f.charAt( 0 ).toUpperCase() + f.substr( 1 ).toLowerCase()
	);
}

function generateCountryMotto( rng )
{
	if ( rng.Scalar() <= 0.25 ) return 'No Official Motto';
	let form = rng.Choose( COUNTRY_MOTTO_FORMS );
	form     = form.replaceAll( '%n', () => rng.Choose( COUNTRY_MOTTO_NOUNS ) );
	form     = form.replaceAll( '%v', () => rng.Choose( COUNTRY_MOTTO_NOUNS ) );
	form     = form.replaceAll( '%a', () => rng.Choose( COUNTRY_MOTTO_ADJECTIVES ) );
	return form.replace(
		/\w\S*/g, ( f ) => f.charAt( 0 ).toUpperCase() + f.substr( 1 ).toLowerCase()
	);
}

function drawFlagStripes( ctx, horiz, colours, width, height )
{
	// draw them
	let stripeSize = horiz ? height / colours.length : width / colours.length;
	for ( let stripe = 0; stripe < colours.length; stripe++ )
	{
		ctx.fillStyle = colours[stripe];

		ctx.fillRect(
			horiz ? 0 : Math.floor( stripe * stripeSize ),
			horiz ? Math.floor( stripe * stripeSize ) : 0,
			horiz ? width : Math.ceil( stripeSize ),
			horiz ? Math.ceil( stripeSize ) : height,
		);
	}
}

function drawFlagTriangle( ctx, colour, size, side, width, height )
{
	ctx.fillStyle = colour;
	ctx.beginPath();

	switch ( side )
	{
		case 0: // LEFT
			ctx.moveTo( 0, 0 );
			ctx.lineTo( width * size, height * 0.5 );
			ctx.lineTo( 0, height );
			break;
		case 1: // RIGHT
			ctx.moveTo( width, 0 );
			ctx.lineTo( width * ( 1.0 - size ), height * 0.5 );
			ctx.lineTo( width, height );
			break;
		case 2: // UP
			ctx.moveTo( 0, 0 );
			ctx.lineTo( width * 0.5, height * size );
			ctx.lineTo( width, 0 );
			break;
		case 3: // DOWN
			ctx.moveTo( 0, height );
			ctx.lineTo( width * 0.5, height * ( 1.0 - size ) );
			ctx.lineTo( width, height );
			break;
	}

	ctx.closePath();
	ctx.fill();
}

function drawFlagCross( ctx, bg, fg, thickness, width, height )
{
	ctx.strokeStyle = bg;
	ctx.lineWidth   = thickness;

	// Draw the cross
	ctx.beginPath();
	ctx.moveTo( 0, 0 );
	ctx.lineTo( width, height );
	ctx.moveTo( 0, height );
	ctx.lineTo( width, 0 );
	ctx.closePath();
	ctx.stroke();

	// If bg != fg, we want an internal line.
	if ( bg != fg )
	{
		ctx.strokeStyle = fg;
		ctx.lineWidth /= 2.0;

		ctx.beginPath();
		ctx.moveTo( 0, 0 );
		ctx.lineTo( width, height );
		ctx.moveTo( 0, height );
		ctx.lineTo( width, 0 );
		ctx.closePath();
		ctx.stroke();
	}
}

function drawFlagPlus( ctx, bg, fg, offX, offY, thickness, width, height )
{
	let crossX = width * ( 0.5 + offX );
	let crossY = height * ( 0.5 + offY );

	ctx.strokeStyle = bg;
	ctx.lineWidth   = thickness;

	// Draw the +
	ctx.beginPath();
	ctx.moveTo( 0, crossY );
	ctx.lineTo( width, crossY );
	ctx.moveTo( crossX, 0 );
	ctx.lineTo( crossX, height );
	ctx.closePath();
	ctx.stroke();

	// If bg != fg, we want an internal line.
	if ( bg != fg )
	{
		ctx.strokeStyle = fg;
		ctx.lineWidth /= 2.0;

		ctx.beginPath();
		ctx.moveTo( 0, crossY );
		ctx.lineTo( width, crossY );
		ctx.moveTo( crossX, 0 );
		ctx.lineTo( crossX, height );
		ctx.closePath();
		ctx.stroke();
	}
}

function drawFlagTris( ctx, dir, col1, col2, width, height )
{
	ctx.fillStyle = col1;

	ctx.beginPath();
	if ( dir )
	{
		ctx.moveTo( 0, 0 );
		ctx.lineTo( width, height );
		ctx.lineTo( 0, height );
	}
	else
	{
		ctx.moveTo( width, 0 );
		ctx.lineTo( 0, height );
		ctx.lineTo( width, height );
	}
	ctx.closePath();
	ctx.fill();

	ctx.fillStyle = col2;

	ctx.beginPath();
	if ( dir )
	{
		ctx.moveTo( 0, 0 );
		ctx.lineTo( width, height );
		ctx.lineTo( width, 0 );
	}
	else
	{
		ctx.moveTo( width, 0 );
		ctx.lineTo( 0, 0 );
		ctx.lineTo( 0, height );
	}
	ctx.closePath();
	ctx.fill();
}

function drawFlag( ctx, rng, width, height )
{
	// Setup clipping...
	ctx.beginPath();
	ctx.rect( 0, 0, width, height );
	ctx.closePath();
	ctx.clip();

	// Figure out style
	let style = rng.GetInt( 0, FLAG_STYLE_MAX );

	switch ( style )
	{
		case FLAG_STYLE_STRIPED: { // Like far, far too many flags
			let horiz   = rng.Scalar() >= 0.5;
			let stripes = rng.GetInt( 2, 8 );

			// generate colours
			let colours = [...Array( stripes )].map( () => randomColour( rng ) );
			drawFlagStripes( ctx, horiz, colours, width, height );

			// Small chance we'll draw a triangle
			if ( rng.Scalar() <= 0.25 )
			{
				let triSize = rng.ChooseWeighted( [0.5, 1 / 3.0, 0.25], [6, 4, 1] );
				let triSide = rng.GetInt( 0, 3 );

				drawFlagTriangle( ctx, randomColour( rng ), triSize, triSide, width, height );
			}
		}
		break;

		case FLAG_STYLE_CROSS: { // Like Scotland
			let bg     = randomColour( rng );
			let linebg = randomColour( rng );
			let linefg = linebg;
			if ( rng.Scalar() <= 0.25 ) linefg = randomColour( rng );

			ctx.fillStyle = bg;
			ctx.fillRect( 0, 0, width, height );
			drawFlagCross(
				ctx, linebg, linefg, rng.Choose( [1, 2, 2.5] ) * ( width / 16 ), width, height
			);
		}
		break;

		case FLAG_STYLE_PLUS: { // Like Denmark, Sweden...
			let bg     = randomColour( rng );
			let linebg = randomColour( rng );
			let linefg = linebg;
			if ( rng.Scalar() <= 0.25 ) linefg = randomColour( rng );

			let horiz = rng.Scalar() >= 0.25; // If horiz, the offset is applied on the X
			let offs  = rng.ChooseWeighted(
                [0.0, -0.25, 0.25, -0.5, 0.5],
                [6, 4, 1, 1, 1],
            )

			ctx.fillStyle = bg;
			ctx.fillRect( 0, 0, width, height );
			drawFlagPlus(
				ctx,
				linebg,
				linefg,
				horiz ? offs : 0,
				horiz ? 0 : offs,
				rng.Choose( [1, 2, 2.5] ) * ( width / 16 ),
				width,
				height
			);
		}
		break;

		case FLAG_STYLE_JACKOFF: { // Literally just the Union Jack, I think.
			let bg        = randomColour( rng );
			let linebg    = randomColour( rng );
			let linefg    = linebg;
			let thickness = rng.Choose( [1, 2, 2.5] ) * ( width / 16 );
			if ( rng.Scalar() <= 0.25 ) linefg = randomColour( rng );

			ctx.fillStyle = bg;
			ctx.fillRect( 0, 0, width, height );
			drawFlagCross( ctx, linebg, linefg, thickness, width, height );

			// Small chance to alternate the colours of the +
			if ( rng.Scalar() <= 0.25 )
			{
				linefg = linebg = randomColour( rng );
				if ( rng.Scalar() <= 0.25 ) linefg = randomColour( rng );
			}

			let horiz = rng.Scalar() >= 0.25; // If horiz, the offset is applied on the X
			let offs  = rng.ChooseWeighted(
                [0.0, -0.25, 0.25, -0.5, 0.5],
                [6, 4, 1, 1, 1],
            )

			drawFlagPlus(
				ctx, linebg, linefg, horiz ? offs : 0, horiz ? 0 : offs, thickness, width, height
			);
		}
		break;

		case FLAG_STYLE_TRIS: { // Like ancap
			drawFlagTris(
				ctx, rng.Scalar() > 0.5, randomColour( rng ), randomColour( rng ), width, height
			);
		}
		break;
	}
}

function genNation( seed )
{
	if ( seed in FLAG_EASTEREGGS )
	{
		let width  = FLAG_WIDTH;
		let height = width / FLAG_RATIOS[2];

		flagCanvas.width  = width;
		flagCanvas.height = height;
		flagCtx.clearRect( 0, 0, width, height );

		let name                     = FLAG_EASTEREGGS[seed]( flagCtx, width, height );
		countryNameDisplay.innerText = `The secret '${name}' flag.`;
		return;
	}

	let flagRNG = new RNG( stringToSeed( seed ) || Date.now() );

	// Job 1: find the aspect ratio
	let ratio = flagRNG.ChooseWeighted( FLAG_RATIOS, FLAG_RATIO_WEIGHTS );
	console.log( ratio );

	// resize canvas to aspect ratio
	let width  = FLAG_WIDTH;
	let height = width / ratio;

	flagCanvas.width  = width;
	flagCanvas.height = height;
	flagCtx.clearRect( 0, 0, width, height );

	// Draw it
	drawFlag( flagCtx, flagRNG, width, height );

	let name = generateCountryName( flagRNG );

	console.log( name );

	countryNameDisplay.innerText =
		`Flag of the nation of '${name}' (${generateCountryMotto( flagRNG )})`;
	// Small chance to be colonised, and colonising the coloniser...
	while ( flagRNG.Scalar() < 0.05 && width > 16 )
	{
		let coloniser = generateCountryName( flagRNG );
		countryNameDisplay.innerText +=
			`, colonised by '${coloniser}' (${generateCountryMotto( flagRNG )})`;

		// draw the small coloniser flag
		drawFlag( flagCtx, flagRNG, width /= 2.0, width / 2.0 );
	}
}

let d = Date.now();
genNation( d );
seedInput.value = d;