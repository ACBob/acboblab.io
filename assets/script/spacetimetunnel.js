'use strict';

/*
Not an experiment in itself perse, but can be used by other places.
*/

function lerp( x, y, t ) { return x * ( 1 - t ) + y * t; }

class SpaceTunnelContext
{
	canvas = null;
	ctx    = null;

	lines  = [];
	clocks = [];

	frame = 0;

	constructor( canvas )
	{
		this.canvas = canvas;
		this.ctx    = canvas.getContext( '2d', { alpha: false } );

		// generate lines as a list of [[x,y], [x,y], [x,y]] edges that we lerp
		// between
		// 128 lines, each with 4 positions
		for ( let i = 0; i < 128; i++ )
		{
			let frames = [];
			for ( let j = 0; j < 4; j++ )
			{
				frames.push( {
					x: Math.random(),
					y: Math.random(),
					w: Math.random() * 0.5 + .75,
				} );
			}
			this.lines.push( frames )
		}

		// generate clocks
		for ( let i = 0; i < 32; i++ )
		{
			// normalized dir
			let dx = Math.random() - 0.5, dy = Math.random() - 0.5;
			const dl = Math.sqrt( dx * dx + dy * dy );
			dx /= dl;
			dy /= dl;

			this.clocks.push( {
				idx: Math.floor( Math.random() * i ),
				x: 0,
				y: 0,
				dx: dx,
				dy: dy,
				speed: Math.random() * 0.25 + .125,
				rot: Math.random() * Math.PI,
				scale: Math.random() * 3 + .1,
			} );
		}

		console.log( this.lines );
	}

	// https://stackoverflow.com/questions/5353934/check-if-element-is-visible-on-screen
	// https://stackoverflow.com/questions/19669786/check-if-element-is-visible-in-dom#21696585
	visible()
	{
		if ( this.canvas.offsetParent === null ) return false;

		const rect       = this.canvas.getBoundingClientRect();
		const viewHeight = Math.max( document.documentElement.clientHeight, window.innerHeight );
		return !( rect.bottom < 0 || rect.top - viewHeight >= 0 );
	}

	draw( dt )
	{
		// don't try drawing if invisible
		if ( !this.visible() ) return;

		this.ctx.fillStyle = 'black';
		this.ctx.fillRect( 0, 0, this.canvas.width, this.canvas.height );

		// draw lines from centre to edges at random points
		const centerX = this.canvas.width / 2;
		const centerY = this.canvas.height / 2;

		this.frame += dt * 0.5;

		const frame     = Math.floor( this.frame );
		const nextframe = Math.floor( this.frame + 1 );
		const lerper    = this.frame - frame;

		this.ctx.strokeStyle = 'white';
		this.ctx.lineWidth   = 15;
		this.lines.forEach( ( line ) => {
			// lerp the positions to the next frame
			const pos = {
				x: lerp( line[frame % line.length].x, line[nextframe % line.length].x, lerper ),
				y: lerp( line[frame % line.length].y, line[nextframe % line.length].y, lerper ),
				w: lerp( line[frame % line.length].w, line[nextframe % line.length].w, lerper ),
			};

			this.ctx.lineWidth = 15 + 5 * pos.w;

			this.ctx.beginPath();
			this.ctx.moveTo( centerX, centerY );
			this.ctx.lineTo( pos.x * this.canvas.width, pos.y * this.canvas.height );
			this.ctx.stroke();
		} );

		// Draw clocks
		if ( CLOCKS.complete )
		{
			this.ctx.imageSmoothingEnabled = false;

			this.clocks.forEach( ( clock ) => {
				const x  = centerX + clock.x * this.canvas.width;
				const y  = centerY + clock.y * this.canvas.height;
				const sz = clock.scale * 64;
				clock.rot += dt * clock.speed + Math.PI / 50.0;

				this.ctx.save();
				this.ctx.translate( x, y );
				this.ctx.rotate( clock.rot );
				this.ctx.drawImage( CLOCKS, clock.idx * 32, 0, 32, 32, -sz / 2, -sz / 2, sz, sz );
				this.ctx.restore();

				clock.x += clock.dx * clock.speed * dt;
				clock.y += clock.dy * clock.speed * dt;

				if ( clock.x > 1 || clock.x < -1 || clock.y > 1 || clock.y < -1 )
				{
					clock.x = 0;
					clock.y = 0;

					clock.dx = Math.random() - 0.5, clock.dy = Math.random() - 0.5;

					// normalize dir
					const dl = Math.sqrt( clock.dx * clock.dx + clock.dy * clock.dy );
					clock.dx /= dl, clock.dy /= dl;
				}
			} );
		}
	}
}

const CLOCKS = new Image();

document.addEventListener( 'DOMContentLoaded', () => {
	// if canvas isn't supported don't bother at all
	// https://stackoverflow.com/a/28153041
	if ( !document.createElement( 'canvas' ).getContext ) return;
	CLOCKS.src = '/clocks.png';

	let spaceTimeTunnels = Array.from( document.getElementsByClassName( 'spacetime' ) );

	spaceTimeTunnels.forEach(
		tunnelCanvas => { tunnelCanvas.spaceCtx = new SpaceTunnelContext( tunnelCanvas ); }
	)

	let last = 0;
	function frame( time )
	{
		requestAnimationFrame( frame );
		const DT = ( time - last ) / 1000.0;
		last     = time;

		spaceTimeTunnels.forEach( tunnelCanvas => {
			if ( tunnelCanvas.getAttribute( 'zzfull' ) )
			{
				tunnelCanvas.width  = window.innerWidth;
				tunnelCanvas.height = window.innerHeight;
			}

			tunnelCanvas.spaceCtx.draw( DT );
		} );
	}
	requestAnimationFrame( frame );
} );