'use strict';

// 1-5 stars on each question
const RATINGS = [
    "Not at all",
    "A little",
    "Moderately",
    "Very much",
    "Completely"
];

// https://stackoverflow.com/a/5650012
function remap(value, low1, high1, low2, high2) {
    return low2 + (high2 - low2) * (value - low1) / (high1 - low1);
}

let app = document.app || {
    // Questions we ask,
    // { "question": "???", "vibes": { "vibe": 0.5 } }
    questions: [],
    // { "vibe": { "label": "vibe name" } }
    vies: {},

    // lowest possible values of vibes
    minVibes: {},
    // highest possible values of vibes
    maxVibes: {},

    elements: {
        quiz: null,
        questions: null,
        results: null,
        count: null,

        vibeBars: {},
    },

    init: function () {
        this.elements.quiz = document.getElementById('questionnaire');
        this.elements.questions = document.getElementById('questions');
        this.elements.results = document.getElementById('resultPercents');
        this.elements.count = document.getElementById('questionCount');

        this.elements.quiz.addEventListener('submit', (e) => {
            e.preventDefault();
            document.app.tally();
        });

        // Now load quiz
        fetch('/experiment/kinkly/quiz.json5')
            .then(response => {
                if (!response.ok) {
                    alert(`Failed to load quiz, HTTP ${response.status}, ${response.statusText}`);
                    throw new Error(`Failed to load quiz ${response.status}`);
                }
                return response.text();
            })
            .then(JSON5.parse)
            .then(data => {
                const { questions, vibes } = data;
                Object.assign(document.app, { questions, vibes });
                document.app.loaded();
            })
            .catch(error => console.error('Error loading quiz:', error));
    },

    generateRatingBar: function (id) {
        const list = document.createElement('ul');
        list.classList.add('rating');

        for (var i = 0; i < RATINGS.length; i++) {
            const rating = document.createElement('li');
            rating.innerHTML = `<input type="radio" name="${id}" value="${i}" title="${RATINGS[i]}" required>`;
            list.appendChild(rating);
        }

        return list;
    },

    generateQuestionElement: function (index, question) {
        const ask = document.createElement('li');
        const label = document.createElement('p');
        label.innerText = question.question;
        ask.appendChild(label);
        ask.appendChild(this.generateRatingBar(index));

        return ask;
    },

    loaded: function () {
        this.elements.count.innerText = this.questions.length;
        const fragment = document.createDocumentFragment();
        this.questions.forEach((question, i) => {
            const element = this.generateQuestionElement(i, question);
            fragment.appendChild(element);

            for (const [vibe, quality] of Object.entries(question.vibes)) {
                if (!(vibe in this.vibes)) {
                    console.warn('Invalid vibe ' + vibe + ' for question "' + question.question + '"');
                    continue;
                }
                const adjustment = quality * (RATINGS.length - 1);
                this.maxVibes[vibe] = Math.max(this.maxVibes[vibe] ?? 0, (this.maxVibes[vibe] ?? 0) + adjustment);
                this.minVibes[vibe] = Math.min(this.minVibes[vibe] ?? 0, (this.minVibes[vibe] ?? 0) + adjustment);
            }
        });

        this.elements.questions.appendChild(fragment);

        const resultFragment = document.createDocumentFragment();
        Object.keys(this.vibes).forEach(vibe => {
            const item = document.createElement('li');

            const percent = document.createElement('progress');
            percent.setAttribute('max', 100);
            percent.setAttribute('value', 0);

            const label = document.createElement('p');
            label.innerText = this.vibes[vibe].label;
            item.appendChild(label);
            item.appendChild(percent);

            resultFragment.appendChild(item);
            this.elements.vibeBars[vibe] = percent;
        });

        resultPercents.appendChild(resultFragment);
    },

    tally: function () {
        const data = new FormData(this.elements.quiz);
        let tally = {};

        for (const [index, rating] of data) {
            const { question, vibes } = this.questions[index];
            const strength = Number.parseInt(rating);

            console.log(question.question, ":", rating);

            for (const [vibe, quality] of Object.entries(vibes)) {
                tally[vibe] ??= 0;
                tally[vibe] += strength * quality;
            }
        }

        console.log(tally);

        // Now we set the progress
        Object.keys(this.vibes).forEach(
            vibe => this.elements.vibeBars[vibe].setAttribute('value',
                remap(tally[vibe], this.minVibes[vibe], this.maxVibes[vibe], 0, 100))
        );
    }
};
document.app = app;
document.app.init();

function randomizeQuizSelections() {
    document.app.questions.forEach((question, index) => {
        const randomRating = Math.floor(Math.random() * RATINGS.length);
        const input = document.app.elements.quiz.querySelector(`input[name="${index}"][value="${randomRating}"]`);
        input.checked = true;
    });

    /// ....and tally
    document.app.tally();
}
