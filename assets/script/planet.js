import * as acbobgl from '/script/acbobgl.js';

// TODO: How would you plaster this into a larger WebGL program?
// TODO: Content handling, i.e loading shaders from different sources

// TODO: In order to get rid of the pixels requirement, I got rid of dithering.
// I want dithering back.

// Changes:
//  - Removed godot-specific stuff
//  - Removed Dithering
//  - Replaced light_distance1/light_distance2 with light_distance vector2
const planetShader = `
precision mediump float;

uniform float rotation;
uniform vec2 light_origin;
uniform vec2 light_distance;
uniform float time_speed;
uniform sampler2D colors;
uniform float size;
uniform float seed;
uniform float time;

varying vec2 UV;

const int OCTAVES = 7;

float round(float x) {
	return float(int(x));
}

float rand(vec2 coord) {
	// land has to be tiled
	// tiling only works for integer values, thus the rounding
	// it would probably be better to only allow integer sizes
	// multiply by vec2(2,1) to simulate planet having another side
	coord = mod(coord, vec2(2.0,1.0)*round(size));
	return fract(sin(dot(coord.xy ,vec2(12.9898,78.233))) * 43758.5453 * seed);
}

float noise(vec2 coord){
	vec2 i = floor(coord);
	vec2 f = fract(coord);

	float a = rand(i);
	float b = rand(i + vec2(1.0, 0.0));
	float c = rand(i + vec2(0.0, 1.0));
	float d = rand(i + vec2(1.0, 1.0));

	vec2 cubic = f * f * (3.0 - 2.0 * f);

	return mix(a, b, cubic.x) + (c - a) * cubic.y * (1.0 - cubic.x) + (d - b) * cubic.x * cubic.y;
}

float fbm(vec2 coord){
	float value = 0.0;
	float scale = 0.5;

	for(int i = 0; i < OCTAVES ; i++){
		value += noise(coord) * scale;
		coord *= 2.0;
		scale *= 0.5;
	}
	return value;
}

vec2 rotate(vec2 coord, float angle){
	coord -= 0.5;
	coord *= mat2(vec2(cos(angle),-sin(angle)),vec2(sin(angle),cos(angle)));
	return coord + 0.5;
}

vec2 spherify(vec2 uv) {
	vec2 centered= uv *2.0-1.0;
	float z = sqrt(1.0 - dot(centered.xy, centered.xy));
	vec2 sphere = centered/(z + 1.0);
	return sphere * 0.5+0.5;
}


void main() {
	//pixelize uv
	vec2 uv = UV;

	// cut out a circle
	float d_circle = distance(uv, vec2(0.5));
	float a = step(d_circle, 0.49999);

	uv = spherify(uv);

	// check distance distance to light
	float d_light = distance(uv , vec2(light_origin));

	uv = rotate(uv, rotation);

	// noise
	float f = fbm(uv*size+vec2(time*time_speed, 0.0));

	// remap light
	d_light = smoothstep(-0.3, 1.2, d_light);

	if (d_light < light_distance.x) {
		d_light *= 0.9;
	}
	if (d_light < light_distance.y) {
		d_light *= 0.9;
	}


	float c = d_light*pow(f,0.8)*3.5; // change the magic nums here for different light strengths

	// now we can assign colors based on distance to light origin
	float posterize = floor(c*4.0)/4.0;
	vec4 col = texture2D(colors, vec2(posterize, 0.0));

	// vec4 col = vec4(posterize,posterize,posterize, 1.0);

	gl_FragColor = vec4(col.rgb, a * col.a);
}
`;

main();

function main()
{
	'use strict';

	/** @type {HTMLCanvasElement} */
	const canvas = document.getElementById( 'planet-place' );
	const gl     = canvas.getContext( 'webgl' );

	if ( !gl )
	{
		alert( 'No WebGL Context found. Is it supported?' );

		// Try and display an error on the 2D canvas api
		const ctx = canvas.getContext( '2d' );
		if ( ctx )
		{
			ctx.fillStyle = 'rgb(255, 255, 255);'
			ctx.fillRect( 0, 0, canvas.width, canvas.height );
			ctx.fillStyle = 'rgb(0, 0, 0);'
			ctx.font      = '24pt serif';
			ctx.fillText( 'No WebGL!', 0, 0, 24 );
		}

		return;
	}

	acbobgl.initWebGL( gl );
	gl.blendFunc( gl.SRC_ALPHA, gl.ONE_MINUS_SRC_ALPHA );
	gl.enable( gl.BLEND );

	const vertexShaderSource   = `
	precision lowp float;
	attribute vec2 position;

	varying vec2 UV;

	void main() {
		gl_Position = vec4(position, 0, 1);
		UV = gl_Position.xy * 0.5 + 0.5;
		UV.y = 1.0 - UV.y;
	}
	`;
	const fragmentShaderSource = `
	precision lowp float;
	varying vec2 UV;
	uniform sampler2D tex;

	void main() {
		gl_FragColor = texture2D(tex, UV);
	}`;

	const vertexData = [
		// First triangle
		1.0,
		1.0, //
		-1.0,
		1.0, //
		-1.0,
		-1.0, //

		// Second Triangle
		-1.0,
		-1.0, //
		1.0,
		-1.0, //
		1.0,
		1.0, //
	];

	const program = acbobgl.compileShader( gl, vertexShaderSource, planetShader );

	const buffer = gl.createBuffer();
	gl.bindBuffer( gl.ARRAY_BUFFER, buffer );
	gl.bufferData( gl.ARRAY_BUFFER, new Float32Array( vertexData ), gl.STATIC_DRAW );
	gl.enableVertexAttribArray( 0 );
	gl.vertexAttribPointer( 0, 2, gl.FLOAT, false, 0, 0 );

	gl.useProgram( program );
	gl.uniform2f( gl.getUniformLocation( program, 'light_origin' ), 0.3, 0.4 );

	gl.uniform2f( gl.getUniformLocation( program, 'light_distance' ), 0.362, 0.525 );

	gl.uniform1f( gl.getUniformLocation( program, 'size' ), 8 );
	gl.uniform1f( gl.getUniformLocation( program, 'seed' ), 1.175 );
	gl.uniform1f( gl.getUniformLocation( program, 'time_speed' ), 0.5 );

	// We create the gradient here.
	let gradTex = gl.createTexture();
	gl.bindTexture( gl.TEXTURE_2D, gradTex );
	gl.texImage2D( gl.TEXTURE_2D, 0, gl.RGB, 5, 1, 0, gl.RGB, gl.UNSIGNED_BYTE, new Uint8Array( [
					   255,
					   137,
					   51, //
					   230,
					   69,
					   57, //
					   173,
					   47,
					   69, //
					   82,
					   51,
					   63, //
					   61,
					   41,
					   54, //
				   ] ) );

	gl.texParameteri( gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE );
	gl.texParameteri( gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE );
	gl.texParameteri( gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR );
	gl.texParameteri( gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR );

	let then = Date.now();
	let time = 0.0;
	function render()
	{
		gl.clear( gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT );
		gl.useProgram( program );
		// gl.bindTexture(gl.TEXTURE_2D, texture);
		gl.drawArrays( gl.TRIANGLES, 0, 6 );

		let now   = Date.now();
		let delta = now - then;

		time += delta / 1000;
		gl.uniform1f( gl.getUniformLocation( program, 'time' ), time );

		then = now;
		requestAnimationFrame( render );
	}

	requestAnimationFrame( render );
}