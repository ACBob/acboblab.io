'use strict';

let acbobSnow = document.acbobSnow || {
	// Where we render to.
	/** @type {HTMLCanvasElement} */
	canvas: null,

	/** @type {CanvasRenderingContext2D} */
	context: null,

	// Used for rendering!
	time: 0.0,

	// Used to offset the snowflakes such that they're not always the same every
	// run.
	variation: Date.now(),

	// How many snowflakes to render
	snowflakes: 64,

	// How hard the wind blows on the X axis.
	// + = right
	// - = left
	wind: 0.15,

	// How strong gravity is
	gravity: 0.5,

	// How big snowflakes should be, in percent of largest screen resolution
	// component.
	flakeSize: 0.5,

	// How much flakes should vary in size
	flakeVariation: 0.5,

	// The style to draw flakes with.
	flakeStyle: 'rgba(255, 255, 255)',
	flakeOutline: 'rgba(0, 0, 0)', // NOTE: can also be null to disable!

	// This controls the resolution scale of the screen.
	resolutionScale: 1.0,

	// This updates the value of time to render nicely.
	lastTick: Date.now(),
	tickRepeater: null,
	targetRate: 30, // 30 FPS.
	tick: function() {
		this.tickRepeater = window.setTimeout( () => {
			document.acbobSnow.tick();
			document.acbobSnow.draw();
		}, Math.floor( 1000 / this.targetRate ) );

		let now = Date.now();
		let dt  = now - this.lastTick;
		this.time += dt / 1000.0;
		this.lastTick = now;
	},

	init: function() {
		this.canvas = document.createElement( 'canvas' );
		// NOTE: I don't know if this is neccesarily bad practice. I don't much
	    // care, neither.
		this.canvas.style.position = 'fixed';
		this.canvas.style.width = this.canvas.style.height = '100%';

		// hide ourselves as much as possible
		this.canvas.style.pointerEvents = 'none';

		// We want to make sure our resolution is correct.
		this.context = this.canvas.getContext( '2d' );
		this.updateSize();
		window.addEventListener( 'resize', () => { document.acbobSnow.updateSize(); } );

		document.body.appendChild( this.canvas );
		this.tickRepeater = window.setTimeout( () => {
			document.acbobSnow.tick();
			document.acbobSnow.draw();
		}, Math.floor( 1000 / this.targetRate ) );
	},

	updateSize: function() {
		this.canvas.width  = document.body.clientWidth * this.resolutionScale;
		this.canvas.height = document.body.clientHeight * this.resolutionScale;

		this.draw();
	},

	draw: function() {
		// Draw snow...
		/** @type {CanvasRenderingContext2D} */
		this.context;

		// this.context.fillRect(0, 0, 200, 200);
		this.context.clearRect( 0, 0, this.canvas.width, this.canvas.height );

		// this.context.beginPath();
	    // this.context.arc(0, 0, 64.0, 0, Math.PI * 2);
	    // this.context.closePath();
	    // this.context.fill();

		// We want to transform the coordinates to basically be universal between
	    // screen sizes. Changing size should adjust resolution, basically.
		const flakeRadius =
			Math.max( this.canvas.width, this.canvas.height ) * ( this.flakeSize / 100.0 );

		const bottom = this.canvas.height + flakeRadius;
		const right  = this.canvas.width + flakeRadius;

		for ( let i = 0; i < this.snowflakes; i++ )
		{
			/* Positioning */
			// To vary attributes between snowflakes.
			const seed = Math.floor( this.variation + i * 1.55 );

			// Varying position slightly.
			const offset = this.pseudoRandom( seed, 0.0, 10.0 );

			// How fast we should travel.
			const speed = this.pseudoRandom( seed, 0.75, 1.0 );

			// How far down the screen we should generally be.
			const verticalPercent = ( offset + ( this.time * speed * this.gravity ) ) % 1.0;

			// How far across the screen we should generally be.
			let horizontalPercent = ( ( i / this.snowflakes ) + this.time * this.wind );
			// we vary slightly to sway.
			horizontalPercent +=
				Math.sin( this.time + seed ) * this.pseudoRandom( seed, 0.01, 0.05 );
			horizontalPercent = horizontalPercent % 1.0;

			if ( horizontalPercent < 0.0 ) horizontalPercent = 1.0 + horizontalPercent;

			/* Screen rendering. */
			let x    = horizontalPercent * right;
			let y    = verticalPercent * bottom;
			let size = flakeRadius * this.pseudoRandom( seed, 1.0 - this.flakeVariation, 1.0 );

			this.context.beginPath();
			this.context.arc( x, y, size, 0, Math.PI * 2 );
			this.context.closePath();

			this.context.fillStyle = this.flakeStyle;
			this.context.fill();

			// If outlines exist, use them
			if ( typeof ( this.flakeOutline ) !== 'undefined' && this.flakeOutline !== null )
			{
				this.context.strokeStyle = this.flakeOutline;
				this.context.stroke();
			}
		}
	},

	pseudoRandom: function( seed, low, high ) {
		let value = Math.tan( seed ) * 10000;
		value     = value - Math.floor( value );
		// Remap from 0 - 1 to low - high
		return low + ( high - low ) * value;
	},
};
document.acbobSnow = acbobSnow;
document.acbobSnow.init();