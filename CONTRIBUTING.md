# Contributing
Despite being a personal site, I do actually accept pull-requests/issues.\
Just shoot me one and we'll have a discussion over GitLab or Email (Your choice) about sorting it out.\

# Making issues
When making an Issue, properly label it pls kthnx.

# Pull requests
Usually, I'll think about adding it if it's helping the site's formatting, fixing spelling mistakes, etc.\
However, There could be situations where I would accept hosting an article, or adding a new page, or what-have-you.\
These could come at any moment, so just shoot me a PR and we'll discuss it there!
