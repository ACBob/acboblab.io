ADDONS=-f scss
OUTPUT_DIR=build

site:
	lektor build $(ADDONS) -O $(OUTPUT_DIR)

run_server:
	lektor server $(ADDONS)

optimise_images:
	find -type f -name "*.png" -exec optipng "{}" \;