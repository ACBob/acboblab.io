# ACBob's ZimZam!
This is my *beautiful* site, in glorious source-code form.\
This also happens to be where it is compiled and deployed from.

# Contributing
No, that's not a left-over from some README template, My personal website does accept Pull requests/Issues.\
Usually this is a case of makin' it and discussing is after the fact. See [CONTRIBUTING](https://gitlab.com/ACBob/acbob.gitlab.io/-/blob/master/CONTRIBUTING.md).

# License
Licensed under the CC-BY-SA-4.0, but it'd be appreciated if you replaced the content of the pages/stuff if you decide to fork, and use in production.

I also may decide to change the license at some point, and this action would be retro-active.
