title: About this Website
---
body:

This is my constantly changing website. Where did *ZimZam* come from? No idea, but it's not a [unique name!](https://duckduckgo.com/?q=%22ZimZam%22)

* Source Code[^1]: https://gitlab.com/ACBob/acbob.gitlab.io
* Site Version: 1.5[^2]
* [About the Developer](/about/me)

### Accessibility & Support
Like a lot of my projects, I've been extremely ambitious with this website.
I've made sure to do things as well as I can. As a rule of thumb, this website should be just fine on any platform that can read HTML files.<br>
Be that Firefox, The Nintendo DSi[^4], or some kind of text-based TTY browser.
Hell, it even supports *Internet Explorer 8+*, which I'm the most proud of.
Want to see me do more?
Please keep it to yourself for my sanity.

In terms of accessibility, this website conforms to the Web Content Accessibility Guidelines (WCAG), and does not rely on Javascript for essential website features.
Additionally, I've been careful to avoid making styling too essential as-well.
I've tried to support screen readers, as well as printers[^3] to make sure there's no [incompatibilities](/about/museum).

[^1]: Master is where the source lies, the prod branch is where the automatic stuff lies to actually host it.
[^2]: Incremented occasionally at my discretion. Frequently forgotten.
[^3]: NOTE: I only tested this with Firefox, Chrome/Opera may handle it differently!
[^4]: Yes, really. The only things that did not work were the [QWC](/comics/qwc/) (animations), and the [experiments](/experiment/). This is probably because the DSi's browser is based on an older version of Opera. This may also mean that my website is supported by the 3DS, Wii, and Wii U.
