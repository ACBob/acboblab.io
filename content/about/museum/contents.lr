_model: page
---
title: Support Museum
---
body:

Welcome to the support museum!
On this page, is a screenshot of my website (with its level of support) on various platforms and browsers.
Take a gander, see how silly I've been.

## Criterion
For my website to be 'supported', I test the following criteria:
 * Can you navigate through (most) of the website? (Most important)
 * Does the website *look* like my site? (Doesn't apply on text-based)
 * Does the main functionality of the website work? (This ignores mobile considerations and any experiments)

A *platform* being supported means that I will/have placed it in my rota of testing with every change, making sure the website works as good as possible for the given platform.
The supported platforms are as follows:
 * Firefox
 * Chromium
 * Printers
 * Screen Readers
 * Text-Based Browsers

## Windows 98
**Internet Explorer 5**, the version that *comes with* Windows 98 supports this website.
Note that images did not load, and I cannot tell if it's because they were PNGs too new, or if it's because I had to load the site from a CD.
(This is technically Windows 98 running through *DosBox-X*, not a native run (naturally) and I couldn't get networking to work.)

![](/img/support/windows98.png)

## Linux Mint
This is a bit cheating, as this is the platform I'm developing on, but I'll count it regardless.

**Firefox 105.0** supports this site, although I think I have some experimental CSS stuff turned on (revolutionary css tags, you get the idea).

![](/img/support/linux-firefox.png)

**Chromium 105.0** supports this site. Also cheating, but not as much, as chromium isn't exactly very good at standards.

![](/img/support/linux-chromium.png)

**Falkon 3.1.0** which is Qt-based, and just uses Qt's web view.
I think that's webkit...

![](/img/support/linux-falkon.png)

## Nintendo Devices
The Nintendo 3DS is an officially supported platform.
There *are* some hiccups, but they are not disastrous to the usability of the website, so I think I've done a good job overall.

![](/img/support/3ds-3ds.png)

The DSi was a platform I thought would be funny to test.
Turns out it's not far from working.
Doesn't make it a supported platform.
I should note that the image given here is not a native DSi, it is a modded 3DS (it's surprisingly easy) but it is the DSi software.

![](/img/support/3ds-dsi.png)

I've not yet had an opportunity to try the Wii nor Wii U, and the switch does not have a browser per-se.

## Android

**Firefox** supports this site.

![](/img/support/android-firefox.png)

## Text-Based browsers
I have only tested a handful of these, but regardless, the website seems fine.

**Lynx**.

![](/img/support/lynx.png)

**Links, Links2.**

![](/img/support/links.png)
