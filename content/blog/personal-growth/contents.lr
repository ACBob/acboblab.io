title: Personal Growth
---
descriptor: I oft call the me of the past A Bad Person, but just how bad were they?
---
body:

I don't have anywhere better to put this, but apparently the [neocities](https://acbobthecat.neocities.org/) mirror of this website has almost **4 million views** and pulls hundreds of people a week - *and* that I'm [the third most viewed blog](https://neocities.org/browse?sort_by=views&tag=blog)[^3rd].
Hello.
That's mildly terrifying.

[^3rd]: as of writing.

I'm not sure what came over me, but I decided to port my [older website](https://acbob.neoctiies.org/)'s articles to this website.
In doing so, I had to convert (by hand) from the stupid JSON-based line-by-line format I used to write them in into Markdown, in a way that still conveyed the original intent (as best I could).
While the format I created was admirable - it had footnote support and some form of macros - it's not the focus of this article as you can probably tell.

I decided, god forbid, to actually *read* what I had written attached to my name.
I'm oft dismissive or generally disconnected from who I call "my younger self", but when reading these articles it's hard to recognize them as ever being my words.
I can assume this is the biggest evidence towards me having grown as a person from what they were.
But I feel I should lecture this previous state of self - one that can't respond, but I should shine a spotlight on them and my current feelings on what I've said.

But, and quite bizarrely I think, it doesn't end at just philosophies or beliefs.
My tastes are radically different - [my review of the Sonic movie](/blog/archival-sonic-the-hedgehog-2020-review) is *scathingly* critical for what it is, and yet now I'd dare to call the movie pretty good.
But it's quite clear here in the review, quoth the younger;
> Then after that, Sonic n' Cop bond for a bit and we're in san Francisco. we're now at the cliché part, and sonic brings us back with "Wanna know how it ends? me too!" which is obviously the funniest part of the movie, causing me and the cinema to fall out of their chairs and water in the eyes to the point they had to pause the performance.

Clichés are fine, there's no problem in having them in your movie.
You're *intentionally* ignoring the comedy you enjoyed to complain about something that's fine, because you thought you could be witty about it.

I think a lot of this is due in part to me still being in my 'edge' phase.
Everyone has an edge phase and it's different for everyone, it's a period where they're excessively edgy for no real reason other than they discovered they could be contradictory assholes.
Some are lucky and do it in private or at a young age, others... not so much.

It also felt like I was forcing a reason to have a blog.
4 years ago (jesus) I didn't really have much to say, that's why in 2019 when I created that variant a blog wasn't on my mind.
Neither was it on my mind for older websites.
If memory serves, I was inspired by other websites on Neocities and the fact *they* had blogs.
It's painfully clear in the 'review' of the Bobiverse books;
> This one i'm not done reading as of writing, and it's even more enjoyable than the first.
>
> [...]
>
> I haven't read this far enough to comment, yet. [...] This blog is starting to feel redundant as i go along, as it feels i'm grasping for things to write about.

Admittedly this isn't a feeling that's gone away, which is easy to see from the huge spans of time between articles on this version of the site, even when it was designed from the get-go to be a blog first.

The most painful article to read (even with the bash at the homeless in the [Splatoon](/blog/archival-splatoon) post) is the [Minecraft](/blog/archival-minecraft) one.
It's needless, it was made the *day* after I brought about the old blog system and I certainly did not have anything to say on the matter.
Instead of substance, there's bashful comments towards the people taking part and their efforts.
As an attempt at humor, there's ableism towards autism[^autism].

While there is admittedly humorous parts[^humor], it's mostly me making fun of the Minecraft community on the basis they were mentally ill and autistic.
Yikes.
> Minecraft Players have Depression.
>
> And it is this depression, that allows them to be very focused on one task. Almost like autism. Infact, it is autism. Because they are all 14+ and that tells me they were vaccinated[^vaccine].


[^autism]: Which must've been internalized at that point, I'm hardly neurotypical.
[^vaccine]: Hindsight hopes this was satire at the time.
[^humor]:  "Minecraft's target audiance is the 14-25 range. Teenager to Young adult. Seems simple enough. So i googled these numbers, and got -11."<br>This is a pretty funny statement, as indeed a literal search of "14-25" would cause google to become a calculator.

And while it would be easy to paint this as 3 year old opinions that I shackled many years ago, it shouldn't go unsaid that these opinions continued even into *this* website.
Take a look at the [tone indicators](/blog/tone-indicators) article from even just last year, it makes fun of neopronoun users[^neopronoun] and certain identities such as Queerplatonic.

[^neopronoun]: Look where that got you, you big it/they.

Until around 2019, I was pretty sheltered in terms of socialization.
I didn't really have the same opportunities for it as others, and it shows with how I would write and understand the world.
I didn't know anyone other than my family and a few peers in public, and I wouldn't meet my first openly queer friend until mid 2021[^queer].
One of the only people I knew at the time was a 4Channer and you can only imagine what their influence was like.

[^queer]: Who would later help me discover myself.

This lack of talking to people contorted how I expressed myself, such as when [I thought I was even remotely equipped enough to talk about Cloudflare](https://acbob.xyz/blog/cloudflare-and-the-internet/).
Or [when I came out and said that I didn't support BLM](https://acbob.xyz/blog/self-reflection-2020/)[^blm] while discussing my political views becoming more radicalized (Yikes)[^yikes].
I wouldn't say I'm perfect or anything of the sort, but I'd at-least say I'm better than, well, that.

[^blm]: Black lives *do* matter by the way.
[^yikes]: At the very least in this one I decide to avoid writing politics, which I succeed in... mostly.

I don't mean to villify myself.
They were someone suffering from [ADHD they refused to accept they had](https://acbob.xyz/blog/in-memoriam-2021/) and still learning to socialize with real people.
These opinions and views were environmental - I may be a scottish individual but our country's still tacked onto [england](https://www.bbc.co.uk/news/uk-63157965).
It doesn't make what was said anything good to say, and it doesn't make that version of myself any less of a Bad Person&trade; for saying them, but it doesn't make that person of the past irredeemable.
And I at-least hope that I have redeemed myself from what I've said in the past.

I know I'm redeemed in *my* eyes.
Even if I don't recognize who they were as me.
I know that as I am now, I am better.
And who I was matters not.
---
pub_date: 2023-09-29
---
tags: reflection
