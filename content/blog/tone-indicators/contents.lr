title: Tone Indicators
---
body:

Tone indicators have been a part of the internet for a long time. Starting with </sarcasm>, /sarcasm and eventually /s. In recent years with discourse being severely limited to <300 characters, coupled with Poe's Law, it's even harder than ever before to decipher a meaning behind a given text. This has led to the (seeming) creation of several and many more tags to help with figuring out what someone's trying to say. Supposedly, this should also help with neurodivergent people, the likes of Autism and the such. Being autistic myself and finding that tone indicators make the text harder to understand, I finally took the time to look into what tone indicators exist now.

And so I did a simple search, and it revealed to me 4 ([1](https://toneindicators.carrd.co/) [2](https://tonetaglist.carrd.co/) [3](https://t0neindicators.carrd.co/) and [4](https://tonetags.carrd.co/)) websites[^1] that each list tags. Below is a table of what I found. Keep in mind, I favored the smaller strings.

| Indicator | Description(s) | Occurrences |
|------------|------------------|-----------------| 
| /j | Joking | 4 |
| /hj | Half-Joking | 4 |
| /s | Sarcastic | 4 |
| /g | Genuine | 4 |
| /srs | Serious | 4 |
| /nsrs | Non-Serious | 2 |
| /pc | Positive Connotation | 4 |
| /neu | Neutral Connotation | 3 |
| /neg | Negative Connotation | 4 |
| /p | Platonic | 4 |
| /r | Romantic | 4 |
| /c | Copypasta | 3 |
| /l | Lyrics | 4 |
| /lh | Light-Hearted | 4 |
| /lu | Little Upset | 4 |
| /nm | Not Mad | 4 |
| /nbh | Nobody Here | 4 |
| /nsb | Not Sub-Tweeting[^2] | 3 |
| /x | Sexual Intent | 4 |
| /nx | Non-Sexual Intent | 4 |
| /rh OR /rt | Rhetorical Question | 4 |
| /t | Teasing | 4 |
| /ij | Inside Joke | 3 |
| /m | Metaphorically | 4 |
| /li | Literally | 4 |
| /hyp | Hyperbole | 4 |
| /f | Fake | 4 |
| /th | Threat | 4 |
| /cb | Click-bait | 3 |
| /! | Excited | 1 |
| /a | Alterous[^3] | 2 |
| /ao | An Order | 1 |
| /av | A Vent | 1 |
| /ay | At You | 2 |
| /br | Bragging | 1 |
| /calm | Calm | 1 |
| /cel | Celebratory | 1 |
| /co | Comforting | 1 |
| /curi | Curious | 1 |
| /fam | Familial | 1 |
| /fl | Flirting | 1 |
| /fx | Flex[^4] | 1 |
| /gentle | Gentle | 1 |
| /gq OR /gen | Genuine Question | 2 |
| /gs | Genuine Suggestion | 1 |
| /info | Information | 1 |
| /irre | Irrelevant | 1 |
| /jk | Just Kidding | 1 |
| /jw | Just Wondering | 1 |
| /nabr | Not a brag | 1 |
| /nfl | Not flirting | 1 |
| /nafx | Not a flex | 1 |
| /nao | Not an order | 1 |
| /nav | Not a vent | 1 |
| /nay | Not at you | 2 |
| /nbr | Not being rude | 2 |
| /nf | Not Fake[^5] | 1 |
| /nmay | Not mad at you | 1 |
| /npa | Not passive aggressive | 1 |
| /ny | Not Yelling | 1 |
| /ot | Off-topic | 2 |
| /pa | Passive Aggressive | 1 |
| /para | Paraphrase | 1 |
| /pf | Playful | 1 |
| /q | Quote | 2 |
| /qp | Queerplatonic[^6] | 1 |
| /sbh | Somebody Here | 1 |
| /sbtw | Subtweeting | 1 |
| /st | Statement | 1 |
| /tan | Tangent | 1 |
| /tic | Tic | 1 |
| /ts | To self | 1 |
| /u | Upset | 1 |
| /unin | Unintentional |  1 |
| /unre | Unrelated | 1 |
| /vu | Very Upset | 1 |
| /w | Warm | 1 |
| /ref | Reference | 1 |
| /nf | Not-Forced (Separate to Not False ) | 1 |
| /ex | Exaggeration | 1 |

Interestingly, I found that some people have a personal set of tone indicators, which to me seems to destroy the purpose of them.

Never the less, that's more than 80 separate tags. 80 tags, with varying degrees of sensible definition, that one may use and one may have to learn in order to understand. Amazingly, out of them, there is only *one* overlap with tag, /nf - Which was defined as both 'Not Fake' and 'Not Forced'. However it seems most of the pages decided to take their own intuition and invent tags, that or the landscape that is the internet truly is insurmountable.  There's many duplicates or synonyms that really shouldn't be needed, /nsrs vs. /s for example - why would you have 'Not Serious' when simply 'Sarcastic' works? There is also a lot of situational ones. /tic in the situation somebody suffers from tics and doesn't remove them when posting, /w for temperature and even /nmay when talking specifically to someone.

Not all indicators are created equal, tags such as /npa or /nbr I will simply call _lying._ If you have to specify that you're not being rude, You're probably being rude, and saying otherwise is either lying to yourself or others. The positive versions of such indicators, like /pa for passive aggressive, also don't make much sense to me. To me, it seems much like it's a proud-ness to be passive aggressive towards someone, boasting it towards the internet. There's a positive 'subtweet' specifier, that denotes you _are_ 'sub-tweeting', when from a glance it looks like a 'subtweet' is off-hand and secretive? Perhaps this reflects some primal drive of humanity to show the world how bad-ass you are. Or there's just trashy people out there.

Any-who, Here's the most common, agreed definitions collected and counted from the different pages I visited. Each of them has 4 occurrences (Or more, but I didn't count same-page duplicates).

| Indicator | Definition |
|------------|-------------|
| /j | Joking |
| /hj | Half Joking |
| /s | Sarcastic |
| /g | Genuine |
| /srs | Serious |
| /pc | Positive Connotation |
| /nc | Negative Connotation |
| /p | Platonic |
| /r | Romantic |
| /l | Lyrics |
| /lh | Light-Hearted |
| /lu | Little Upset |
| /nm | Not Mad |
| /nbh | Nobody Here |
| /x | Sexual Intent |
| /nx | Non-Sexual Intent |
| /rh | Rhetorical |
| /t | Teasing |
| /m | Metaphorically |
| /li | Literally |
| /hyp | Hyperbole |
| /f | Fake |
| /th | Threat |

Only about 20, which is much easier to understand. As a general rule, you should probably use these as they are a common set that many people seem to agree on. They're also decently easy to understand. Of-course, I could leave it here, having found a small set of indicators to use that should keep me out of trouble, but I'm not known for doing things easy. But before that, I have some notes on the above.

Interestingly, the most common tags seem to be either denoting the *lack* of serious intent - /j, /hj, /lh, /s, /f, /t, /m - or denoting the *presence* of serious intent - /srs, /li, /g. There's also tags for relationships - /p, /r - and I'm happy to see that there's a tag for questions that shouldn't be answered - /rh. I'm also happy to see that despite the internet leading to splintering communities, there's still a common language developing. I can see tone indicators quickly becoming a regular part of English text, when academia finally catches up[^7].

## Zamtags
Seeing the failures of the Tone Indicators, Zamtags is my flashy brand-spanking-new Tone Indication system. Behold; the Zamtag proposal:

| Positive Code | Positive Description | Negative Code |  Negative Description | Synonyms |
|-----|----------------------------------------|------|---------------------------|--------------|
| /j  | Joking                                 | /ns  | See Non Sarcasm           |              |
| /s  | Sarcasm                                | /ns  | Non-Sarcasm               | /j & /srs    |
| /g  | Genuine                                |      |                           | /ns          |
| /pc | Positive Connotations                  | /nc  | Negative Connotations     |              |
| /r  | Romance                                | /nr  | Non-Romantic, or Platonic |              | 
| /ot | Off-topic aside, such as a copied text |      |                           |              |
| /a  | Real or True - 'Actually'              | /f   | False or Fake             | /nf          |
| /x  | Sexual                                 | /nx  | Non-Sexual                |              |
| /th | Threat                                 | /nth | Non-Threat                | /ns for /nth |

A specification consisting of 15 strictly defined tags, it defines unique codes for every positive statement. It has allowances for synonyms with a few tags, to keep it a bit more inline with current tone indicators. Keep in mind, Zamtags can be valid Tone Indicators, but Tone Indicators are not valid Zamtags. There is consistency in negation, where any tag may be negated by prefixing it with 'n', although best practice states you should use the negative code listed.

Zamtags have a few official styles:
 * 'Classic' Tone Indicators - /ns
 * &lt;ns&gt;XML-Inspired Tags&lt;/ns&gt;
 * [ns]BBCode Inspired Tags[ns]

In essence, 'Classic' Zamtags are a 'personal' set of tone indicators, though it is defined through research. The syntax of these indicators is *slightly* different than regular tone indicators. To perform an additive set of indication, one simply concatenates them without spaces, i.e something off-topic and romantic would be /r/ot. The most important indicator comes first, with it being considered essential for understanding the text, and any additional indicators do not have a specified order and are up to you.

Zamtags can be placed at the beginning to preface a text, in which case the slash goes on the right side. Additive tags are achieved like normal but with the slash on the right. The tone of a text can be changed half-way through by placing a slash on either side to indicate it is a tag. I think the XML and BBCode tags are rather self-explanatory, but I have still included examples. The scope of a tag can be confusing when the classic syntax is used, so the recommended use of Zamtags is the BBCode.

Examples:
```
A sarcastic comment:
Man, The weather's been great today! /s

<s>Man, the weather's been great today!</s>

[s]Man, the weather's been great today![/s]

A spicy political joke:
j/ I don't really like apples. Never have.

<j>I don't really like apples. Never have.</j>

[j]I don't really like apples. Never have.</j>

A longer text where a few indications of tone are needed:
s/ I really like tone indicators. /ns/ I think they can be useful, but many people abuse them much like certain other labels, /ot/ such as pronouns. /s/ But I'm in full support of ALL tone indicators. Don't worry. /f

<s>I really like tone indicators.</s><ns>I think they can be useful, but many people abuse them much like certain other labels,</ns><ot>such as pronouns.</ot><s>But I'm in full support of ALL tone indicators.</s><f>Don't worry.</f>

[s]I really like tone indicators.[/s][ns]I think they can be useful, but many people abuse them much like certain other labels,[/ns][ot]such as pronouns.[/ot][s]But I'm in full support of ALL tone indicators.[/s][f]Don't worry.[/f]

```


[^1]: With varying degrees of usable styling, but they all used a service called Carrd.
[^2]: According to the urban dictionary, a to subtweet is to make a post on twitter about somebody but without directly mentioning them in the post, so that twitter doesn't give them a notification. Apparently it's in the Merriam Webster dictionary, lovely!
[^3]: Seemingly made-up term for something in-between platonic/romantic
[^4]: At-least one of the pages described this as synonymous with '/br'
[^5]: Otherwise known as True.
[^6]: What?
[^7]: Not that I'm necessarily in support of tone indicators, I'm just happy that humans can still communicate sensibly.
---
descriptor: /srs
---
pub_date: 2022-01-01
---
tags:

not great
humor
